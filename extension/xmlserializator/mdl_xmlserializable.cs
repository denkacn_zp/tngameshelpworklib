﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNKWorkLib.Extension.XMLSerializator
{
    public class mdl_XMLSerializable : IXMLSerializable
    {
        public mdl_XMLSerializable()
        {}

        public void Initialization()
        {
            
        }
        //сереализуем
        public string Serialize<T>() where T : class
        {
            return XMLSerializator<T>.Serialize(this);
        }
        //десериализуем
        public void Deserialize<T>(string data) where T : class
        {
            T serialized_class = XMLSerializator<T>.Deserialize(data);
            XMLSerializator<T>.CopyClassData(serialized_class, this);
        }
    }
}
