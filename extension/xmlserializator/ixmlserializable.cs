﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNKWorkLib.Extension.XMLSerializator
{
    public interface IXMLSerializable
    {
        void Initialization();				                //инициализирует
        string Serialize<T>() where T : class;				//сериализует в xml строку
        void Deserialize<T>(string data) where T : class;   //десириализует в класс
    }
}
