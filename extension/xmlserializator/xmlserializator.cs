﻿using System.Xml.Serialization;
using System.IO;
using System;
using System.Reflection;

namespace DNKWorkLib.Extension.XMLSerializator
{
    /// <summary>
    /// Универсальный сериализатор XML
    /// </summary>
    public static class XMLSerializator<T> where T : class
    {
        static public string Serialize(object o)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            var writer = new StringWriter();
            serializer.Serialize(writer, o);

            return writer.ToString();
        }

        static public T Deserialize(string data)
        {
            var serializer = new XmlSerializer(typeof(T));
            var reader = new StringReader(data);

            return (T)serializer.Deserialize(reader);
        }

        /// <summary>
        /// копирует значение полей класса в в такой же класс.
        /// </summary>
        /// <param name='o_in'>
        /// входящий класс с него данныйе будут скопированы
        /// </param>
        /// <param name='o_out'>
        /// исходящий класс в него данные будут записаны
        /// </param>
        static public void CopyClassData(object o_in, object o_out)
        {
            Type curType = o_out.GetType();
            FieldInfo[] properties = curType.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
            int counter = 0;

            foreach (FieldInfo property in properties)
            {
                try
                {
                    FieldInfo property2 = o_in.GetType().GetField(property.Name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
                    property.SetValue(o_out, property2.GetValue(o_in));
                }
                catch (Exception exception)
                {

                }
                counter++;
            }
        }
    }
}
