﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNKWorkLib.Extension
{
    public static class dnkCommonUtility
    {
        /// <summary>
        /// Переделает List в стринг c разделителем ;
        /// </summary>
        /// <param name="list">Любой лист</param>
        /// <returns>стринг c разделителем ;</returns>
        public static string ListToString(this IList list, char separator)
        {
            StringBuilder result = new StringBuilder("");

            if (list.Count > 0)
            {
                result.Append(list[0].ToString());
                for (int i = 1; i < list.Count; i++)
                    result.AppendFormat(separator + " {0}", list[i].ToString());
            }

            return result.ToString();
        }

        /// <summary>
        /// Переделывает из стирнги в лист<стринг>
        /// </summary>
        /// <param name="data"></param>
        /// <param name="separator">разделитель</param>
        public static void StringToList(this List<string> list, string data, char separator)
        {
            list = new List<string>( data.Split(separator).ToList() );
        }
    }
}
