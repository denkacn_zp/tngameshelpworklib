﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNKWorkLib.Extension.DContainer
{
    static public class DContainerManager<T> where T : class
    {
        static private Dictionary<Type, T> _containerEllements = new Dictionary<Type, T>();
        static public void Registr( T con )
        {
            _containerEllements.Add(typeof(T), con);
        }

        static public T Get(Type ct)
        {
            if (_containerEllements.ContainsKey(ct))
                return _containerEllements[ct];
            else
                return null;
        }
    }
}
